## Apache Kafka
Apache Kafka - это распределённый диспетчер, работающий на основе Zookeeper.

### Терминология и особенности
* Единица представления в Kafka - логи
    * _чем отличаются логи от обычных тестовых строк, какая ключевая особенность их хранения?_
* Семантика доставки сообщений (из коробки) - At most once если репликация асинхронная и At least once если синхронная. С недавнего времени из коробки exactly once.
    * _Какие вы знаете семантики доставки сообщений? Для каких задач предпочительна каждая из них?_
* Асинхронная репликация
* Не хранит порядок сообщений (в отличие от обычных очередей)

Типы репликаций:
1. __Синхронная.__ Вновь пришедшие данные реплицируются синхронно на несколько нод.
    * Пример: Hadoop
2. __Асинхронная.__ Вновь пришедшие данные считаются записанными в систему тогда когда они записаны на 1 или несколько нод. Репликация на остальные ноды происходит уже после этого. Для каждой партиции лидером могут являться *различные ноды*.
    * Пример: Kafka. В данном случае "1 или несколько нод" - это ISR (in-sync replicas, подмножество всех брокеров Kafka).

Асинхронная репликация работает быстрее, но при этом больше вероятность потери данных.

Отличия лидера в Kafka от мастера (NameNode) в Hadoop.
* Мастер контролирует выполнение задач рабочими узлами, но не делает за них вычисления.
* Лидер же имеет функционал рабочего узла, но при этом обладает специфич. свойствами (например, хранит самую свежую версию данных).
* В Kafka чтение сообщения происходит из мастера, остальные ноды нужны только на бекапа.

### Kafka CLI

Сервисы, которые использует Kafka:

| **Service** | **URL** |
|:-------:|:---:|
|Zookeeper servers|mipt-master.atp-fivt.org:2181|
||mipt-node01.atp-fivt.org:2181|
||mipt-node02.atp-fivt.org:2181|
|Kafka brokers|mipt-node04.atp-fivt.org:9092|
||mipt-node05.atp-fivt.org:9092|
||mipt-node06.atp-fivt.org:9092|
||mipt-node07.atp-fivt.org:9092|
||mipt-node08.atp-fivt.org:9092|
|Kafka bootstrap-servers| те же адреса, что и у брокеров|

#### Работа с топиками
Для того, чтоб работать с топиками нужно подключиться к Kafka. Для этого в параметры нужно передать хотя бы одну ноду Zookeeper. Команда обращается к Kafka, получает все сервера Zookeeper и с их помощью ищет нужный топик.

##### Выведем список топиков
```
kafka-topics --zookeeper mipt-node01.atp-fivt.org:2181 --list
```
##### Создадим топик
Для топика нужно определить:
* число партиций,
* фактор репликации,
* название топика.
```
kafka-topics --zookeeper mipt-node01.atp-fivt.org:2181 --create \
    --partitions 6 \
    --replication-factor 2 \
    --topic pd2018XX-topic
```

(вместо `XX` напишите 2 последние цифры своего аккаунта)
Снова делаем `list` и проверим, что топик создался. Максимальное кол-во партиций ограничено кол-вом kafka-брокеров.

##### Вывод информации о топике
Для этого вызываем `--describe`
```
kafka-topics --zookeeper mipt-node01.atp-fivt.org:2181 --describe --topic pd2018XX-topic
```
Что мы видим:
```
Topic:pd2018XX_topic	PartitionCount:6	ReplicationFactor:2	Configs:
	Topic: pd2018XX_topic	Partition: 0	Leader: 135	Replicas: 135,131	Isr: 135,131
	Topic: pd2018XX_topic	Partition: 1	Leader: 131	Replicas: 131,132	Isr: 131,132
	Topic: pd2018XX_topic	Partition: 2	Leader: 132	Replicas: 132,133	Isr: 132,133
	Topic: pd2018XX_topic	Partition: 3	Leader: 133	Replicas: 133,134	Isr: 133,134
	Topic: pd2018XX_topic	Partition: 4	Leader: 134	Replicas: 134,135	Isr: 134,135
	Topic: pd2018XX_topic	Partition: 5	Leader: 135	Replicas: 135,132	Isr: 135,132
```
* 6 партиций,
* 2 реплики у каждой,
* лидер и рабочие узлы для каждой партиции (например, видим, что 0-я и 5-я партиции имеют лидеров на 1 машине, а рабочие ноды у них отличаются)

Лидеры назначаются с помощью [Round-robin](https://ru.wikipedia.org/wiki/Round-robin_(%D0%B0%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC)). При этом, Kafka умеет балансировать нагрузку в зависимости от загруженности нод.

#### Чтение-запись в топик

* consumer читает данные, использует zookeeper
* producer записывает данные, использует kafka-broker

Kafka может читать данные из разных источников (HDFS, выходы приложений, базы данных, ханилище Amazon AWS). Можем также смешивать источники. Мы рассмотрим самый простой источник - консоль.

##### Прочитаем данные из топика
```
kafka-console-consumer --zookeeper mipt-node01.atp-fivt.org:2181 --topic pd2018XX-topic
```
Команда сама не завершится т.к. она читает данные в реальном времени (ждёт, пока что-нибудь запишется в топик). Данных нет, прерываем команду.

##### Запишем данные в топик
Для этого нужно указать не Zookeeper-server, а список Kafka-брокеров. В списке достаточно 1 элемента.
```
kafka-console-producer --broker-list mipt-node05.atp-fivt.org:9092 --topic pd2018XX-topic
```
Запишем последовательно несколько чисел в топик и завершим команду.

Можно записывать всю последовательность одной командой: `seq 20 | kafka-console-producer --broker-list mipt-node05.atp-fivt.org:9092 --topic pd2018XX-topic`

##### Realtime чтение-запись.
Запускаем `tmux` и создаём 2 панели. В одной из них запускаем consumer, в другой - producer.

Записываем числа и видим, что они синхронно отображаются в consumer'e.

Можно писать в один топик из нескольких источников и таким образом получится примитивный аналог TCP-чата.

Завершаем команды.

##### Kafka не хранит время поступления данных
Попробуем ещё раз прочитать данные... и видим, что снова ничего нет :(

По умолчанию consumer выводит только вновь поступившие сообщения, те, которые имеют самый свежий отступ (вспоминаем Offset'ы в Hadoop). Попробуем вывести весь топик. Также в настройках топика можно задать время хранения данных. В таком случае если место заканчивается, более старые данные будут удаляться.
```
kafka-console-consumer --zookeeper mipt-node01.atp-fivt.org:2181 --topic pd2018XX-topic --from-beginning
```
Данные выводятся, но в перемешанном виде (см. начало семинара, Kafka не хранит порядок сообщений в топике).

Мы можем вывести данные из конкретной партиции: 
```
kafka-console-consumer --zookeeper mipt-node01.atp-fivt.org:2181 --topic pd2018XX-topic --from-beginning --partition 0
```
И... не работает.

Дело в том, что для использования этой фичи нужно использовать new-style consumers. В первом приближении они отличаются от обычных консьюмеров тем, что у работают не брокерами, а с bootstrap-серверами. Это более общий интерфейс kafka, который знает и о брокерах, и о ZooKeeper'ах.
```
kafka-console-consumer --bootstrap-server mipt-node06.atp-fivt.org:9092 --topic pd2018XX-topic --new-consumer --from-beginning --partition 0
```
В рамках одной партиции видим упорядоченный результат.

##### User-defined скрипты для Kafka.
* [Java Doc по Kafka API](https://kafka.apache.org/10/javadoc/?org/apache/kafka/),
* [Python API](https://kafka-python.readthedocs.io/en/master/usage.html) тоже есть.

```
kafka-run-class kafka.tools.GetOffsetShell --broker-list  mipt-node05.atp-fivt.org:9092 --topic pd2018XX-topic --time -1
```

# EtcD

```bash
data_root="${HOME}/.etcd.data"
rm -rf "${data_root}" && mkdir -p "${data_root}" || { echo 'error creating storage'; exit 1; }
docker rmi gcr.io/etcd-development/etcd:v3.3.18 || true

docker rm -f etcd-gcr-v3.3.18

docker run -d -u `id -u` \
  -p 2379:2379 \
  -p 2380:2380 \
  --mount type=bind,source="${data_root}",destination=/etcd-data \
  --name etcd-gcr-v3.3.18 \
  gcr.io/etcd-development/etcd:v3.3.18 \
  /usr/local/bin/etcd \
  --name s1 \
  --data-dir /etcd-data \
  --listen-client-urls http://0.0.0.0:2379 \
  --advertise-client-urls http://0.0.0.0:2379 \
  --listen-peer-urls http://0.0.0.0:2380 \
  --initial-advertise-peer-urls http://0.0.0.0:2380 \
  --initial-cluster s1=http://0.0.0.0:2380 \
  --initial-cluster-token tkn \
  --initial-cluster-state new \
  --log-output stderr
```

Далее заходим в контейнер:
```bash
docker exec -it <CONTAINER_ID> /bin/sh
```
/bin/bash работать не будет т.к. на этом контейнере урезанная ОСь.

Примеры команд, кот. можно выполнить внутри контейнера.
```
/usr/local/bin/etcdctl set /foo/bum "bum"

/usr/local/bin/etcdctl watch -r /foo

/usr/local/bin/etcdctl mkdir /bar'

/usr/local/bin/etcdctl set /bar/boo "boo"'
boo

/usr/local/bin/etcdctl set /bar/bunny "baz"'
baz

/usr/local/bin/etcdctl ls /bar
/bar/boo
/bar/bunny
```

Doc: (часть команд не работает из-за несоответствия версий) 
 - https://etcd.io/docs/v3.4.0/dev-guide/interacting_v3/
 - https://www.compose.com/articles/etcd2to3-new-apis-and-new-possibilities/

### EtcD puthon client

Установка: `pip3 install python-etcd` (при запущенном докере)

```python
import etcd
c = etcd.Client(host='localhost', port=2379)
```
Etcd python client: https://python-etcd.readthedocs.io/en/latest/

## Задача на Spark

Напишите программу выводящую на экран TOP 5 стран с наибольшим числом посетителей. Можно считать id посетителя как "ip+user_agent".
Результат должен содержать полное имя страны и число уникальных посетителей из нее, упорядоченные по числу посетителей по убыванию.

#### Входные данные
* Логи посещений: `/data/access_logs/big_log_10000`
  * IP-адрес пользователя (195.206.123.39),
  * Далее идут два неиспользуемых в нашем случае поля (- и -),
  * Время запроса ([24/Sep/2015:12:32:53 +0400]),
  * Строка запроса ("GET /id18222 HTTP/1.1"),
  * HTTP-код ответа (200),
  * Размер ответа (10703),
  * Реферер (источник перехода; "http://bing.com/"),
  * Идентификационная строка браузера (User-Agent; "Mozilla/5.0 (Windows NT 6.1; Win64; x64)  AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.94 Safari/537.36").
* Геолокация: `/data/access_logs/geoiplookup`
  * IP ("194.120.126.123"), 
  * Локация ("GeoIP Country Edition: NL, Netherlands")

```
Netherlands<TAB>10000
France<TAB>1000
Russian Federation<TAB>100
Brazil<TAB>10
```

Подсказка: [groupByKey()](https://spark.apache.org/docs/2.1.0/api/python/pyspark.html#pyspark.RDD.groupByKey).

Парсинг логов
```python
import re
import sys
from pyspark import SparkContext
from pyspark import SparkConf
from pyspark.sql import SparkSession
from datetime import datetime as dt

log_format = re.compile( 
    r"(?P<host>[\d\.]+)\s" 
    r"(?P<identity>\S*)\s" 
    r"(?P<user>\S*)\s"
    r"\[(?P<time>.*?)\]\s"
    r'"(?P<request>.*?)"\s'
    r"(?P<status>\d+)\s"
    r"(?P<bytes>\S*)\s"
    r'"(?P<referer>.*?)"\s'
    r'"(?P<user_agent>.*?)"\s*'
)

def parseLine(line):
    match = log_format.match(line)
    if not match:
        return ("", "", "", "", "", "", "" ,"", "")

    request = match.group('request').split()
    return (match.group('host'), match.group('time').split()[0], \
       request[0], request[1], match.group('status'), match.group('bytes'), \
        match.group('referer'), match.group('user_agent'),
        dt.strptime(match.group('time').split()[0], '%d/%b/%Y:%H:%M:%S').hour)


lines = sc.textFile(DATASET)
parsed_logs = lines.map(parseLine).cache()

geoip_format = re.compile(
    r'"(?P<host>[\d\.]+)",\s'
    r'"(?P<location>.*?)"')

def parseGeoipLine(line):
    match = geoip_format.match(line)
    if not match:
        return ("", "")

    # GeoIP Country Edition: NL, Netherlands
    location = match.group('location')
    if "Address not found" in location:
        return ("", "")
    else:
        return (match.group('host'), location.split(", ")[1])
```

Решение в папке: `/home/velkerr/seminars/pd2020/14-15-spark/practical_tasks/502`.
